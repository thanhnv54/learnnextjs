import Layout from './../components/Layout';
import { useState, useEffect } from "react";
const Clock = () => {

    const [state, setstate] = useState(0);
    const [isOn, setOn] = useState(false);
    const [interId, setInterId] = useState(0)

    
    useEffect(() => {
       if(isOn){
        setInterId(setInterval(()=>setstate((state)=>state+1), 1000));
       }else{
        clearInterval(interId);
       }
       return () => clearInterval(interId);
    },[isOn])

    return (
        <Layout>
            <div>
                <div className="contai">
                    <span className="num">{state}</span>
                    <div className="top">

                        <div className="timer">
                            <svg className="reset" height="80" width="300" onClick={()=>setstate(0)}>
                                <circle cx="300" cy="150" r="150" fill="#096dd9" ></circle>
                                <text className="btnReset" x="240" y="50">&#8635; </text>
                            </svg>
                            {!isOn ?

                                <svg className="btnStart" onClick={() =>setOn(!isOn)} height="80" width="300">
                                    <circle cx="300" cy="150" r="150"  ></circle>
                                    <text className="start"  x="240" y="50" > &#9658; </text>
                                </svg>
                                :
                                <svg className="btnPause" onClick={() => setOn(!isOn)} height="80" width="300">
                                <circle cx="300" cy="150" r="150"  ></circle>
                                <text className="pause" x="240" y="50" >&#9607; </text>
                            </svg>

                            }

                           
                        </div>
                    </div>


                </div>

                <style jsx>{`
                .num{
                    position: absolute;
                    margin-top: 25%;
                    font-size: 100px;
                    z-index: 32423;
                    width: 100%;
                    text-align: center
                }
                .contai {
                    width: 300px;
                    height: 300px;
                    position: relative;
                }
                .top {
                    
                    position: relative;
                    background-color: yellow;
                    position: relative;
                    width: 300px;
                    height:300px;
                    border: 4px solid #e8e8e8;
                    border-radius: 200px;
                    background-color: #fafafa;
                    display: flex; 
                }
              
                .btnPause {
                    cursor: pointer;
                    position: absolute;
                    transform: rotateY(180deg);
                    }
                .btnPause .pause {
                    position: absolute;
                    font-family: sans-serif;
                    font-size: 30px; 
                    fill: white;
                    }

                .btnStart {
                        cursor: pointer;
                        position: absolute;
                        transform: rotateY(180deg);
                        fill: #33cc33
                        }
                .btnStart .start {
                        position: absolute;
                        font-family: sans-serif;
                        font-size: 40px; 
                        fill: white;
                        }
                .reset {
                    cursor: pointer;
                    }
                .btnReset {
                    font-family: sans-serif;
                    font-size: 40px; 
                    fill: white;
                    }
                .timer {
                    margin-left:50%;
                    transform: rotate(180deg)
                    } 
            `}</style>
            </div>
        </Layout>
    );
}



export default Clock;