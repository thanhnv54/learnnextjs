import Layout from './../components/Layout';
import { useState, useEffect } from 'react';
const Clock = () => {
    const key = ['CE', 'C', '◄', '/', 7, 8, 9, '*', 4, 5, 6, '-', 1, 2, 3, '+', '±', 0, '.', '='];
    const func =['CE', 'C', '◄'];
    const oper = ['+','-','*','/']; 
    const num =[1,2,3,4,5,6,7,8,9,0,'.'];
    const [strMath, setStrMath] = useState('');
    const [resul, setResul] = useState('')
    const [histo, setHisto] = useState([]);
    const [viewHis, setViewHis] = useState(false);
    useEffect(()=>{
     if(resul!==''){
        setResul('')
     }
    },[strMath]);

    function funcAction(action){
        if(action==='C'){
            setStrMath('');
            if(resul !==''){
                let obj = {strMth: strMath+' =', reslt: resul}
                setHisto(x=>[...x, obj])
                setResul('');
            }
            
        }
        if(action==='CE'){
            setStrMath('');
            setResul('')
            setHisto([]);
        }
        if(action==='◄'){
            setStrMath(x=>x.substring(0, x.length-1));
            setResul('')
        }
    }

    function operAction(action){
        setStrMath(x=>x+action);
    }

    function result(action){
        
        if(strMath!=='' && !oper.includes(strMath.slice(-1))){
            try {
                let res = eval(strMath);
                let strres =res.toString()
                if(action==='±'){
                    setResul((parseFloat(strres)*(-1)).toString());
                }else{
                    setResul(strres);
                }
            } catch (error) {
                console.log('syntax error!');
            }
            
            
        }
    }
    function viewHistory(){
        setViewHis(!viewHis);
    }
    return (
        <Layout>
            <div className="container">
            <span className="btn-clear-history" style={{zIndex: viewHis?'55':'-21'}} onClick={()=>setHisto([])}>☠</span>
                <div className="view">
                    <div className = "str-math">
                        <span >{strMath}</span>
                    </div>
                   <div className="str-result">
                    <span>{resul.toString()}</span> 
                   </div>
                   
                </div>
                <div className="func-container">
                    <div className="history" onClick={()=>viewHistory()}>
                        <span>History</span>
                        
                    </div>
                    <div className="view-history" style={{zIndex: viewHis?'12':'-21'}}>
                        {histo.map((item,index)=>{
                            return <div className="list-history" key={index}>
                                <span style={{fontSize:'x-large'}}>{item.strMth}</span>
                                <span style={{fontSize:'x-large', fontWeight:'bold'}}>{item.reslt}</span>
                            </div>
                        })}
                    </div>
                    <div className="keyboard">
                        {key.map((item) => {
                            if(num.includes(item)){
                                return <div className='child'  onClick={()=>setStrMath(x=>x+item)}  style={{ backgroundColor: Number.isInteger(item) ? 'white' : '' }} key={item}>{item}</div>
                            }else if(oper.includes(item)){
                                return <div className='child'  onClick={()=>operAction(item)}  style={{ backgroundColor: Number.isInteger(item) ? 'white' : '' }} key={item}>{item}</div>
                            }else if(func.includes(item)){
                                return <div className='child'  onClick={()=>funcAction(item)}  style={{ backgroundColor: Number.isInteger(item) ? 'white' : '' }} key={item}>{item}</div>
                            }else{
                                return <div className='child'  onClick={()=>result(item)}  style={{ backgroundColor: Number.isInteger(item) ? 'white' : '' }} key={item}>{item}</div>
                            }
                    
                        })}
                    </div>
                </div>
            </div>
            <style jsx>{`
            .container{
                width:350px;
                height:500px;
                position: relative;
                border: 1px solid black;
            }
            .view{
                position: absolute;
                height: 20%;
                width: 100%;
                text-align: right;
            }
            .func-container{
                background-color: #f9f2ec;
                position:relative;
                margin-top: 28.5%;
                width: 100%;
                height: 80%;
            }
            .history{
                width: 100%;
                height: 13%;
                border-top: 1px solid #f3e5d8;
                border-bottom: 1px solid #f3e5d8;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .history span{
                font-weight: 600;
            }
            .keyboard{
                display: flex;
                flex-wrap: wrap;
                height: 83%;
                width: 100%;
                justify-content: center;
            }
            .child{
                width: 24%;
                background-color: #f9f2ec;
                border: 1px solid #f3e5d8;
                text-align: center;
                padding-top: 5%;
                font-size: xx-large;
                font-weight: bold;
            }
            .str-math{
                font-size: x-large;
                height: 35%;
                padding-top: 1%;
            }
            .str-result{
                font-size: xx-large;
            }
            .view-history{
                height: 83%;
                width: 100%;
                z-index: 13;
                position: absolute;
                background-color: #f9f2ec;
                overflow: scroll;
                
            }
            .list-history{
                color: black;
                text-align: right;
                height: 20%;
                width: 100%;
                display: grid;
            }
            .btn-clear-history{
                text-align: center;
                font-size: 35px;
                margin-top: 44%;
                position: absolute;
                z-index: 14;
            }   
        `}</style>
        </Layout>
    );
}

export default Clock;