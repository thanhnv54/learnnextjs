import Layout from './../components/Layout';
import { useState, useEffect } from 'react';
const Clock = () => {
    const [state, setstate] = useState(new Date());
    const [show, setshow] = useState(false);
    useEffect(() => {
        setstate(new Date());
    })
    const onChangeValue = () =>{
        setshow(!show)
    }
    const options = { weekday: 'short', day: 'numeric', month: 'long', year: 'numeric' };
    return (
        <Layout>
            <div style={contai}>
            <div style={checkBox}><input type="checkbox" id="show" onChange={onChangeValue}></input><label htmlFor="show">Show Date</label></div>
            <div style={circle}>
                <div >
                    <p style ={time}>{state.getHours()}:{state.getMinutes()}:{seconds_with_leading_zeros(state)}</p>
                    {show? <p style={date}>{state.toLocaleDateString("en-DE", options).replace(",", "")}</p>:''} 
                </div>
            </div>
            </div>
        </Layout>

    );
}

const seconds_with_leading_zeros =(dt) =>
{ 
  return (dt.getSeconds() < 10 ? '0' : '') + dt.getSeconds();
}
const contai={
    height: '600px',
    width: '100%'
}

const checkBox ={
    height:'100px',
    width: '100%',
    textAlign: 'center'
}
const circle = {
    height: '350px',
    width: '350px',
    borderRadius: '50%',
    border: '5px solid',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
    margin: 'auto'
}

const time ={
    fontSize: '90px',
    fontWeight: 100,
    margin: 0,
    color: 'aqua'
}
const date ={
    textAlign: 'center',
    color: 'aqua'
}
export default Clock;