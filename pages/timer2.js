import Layout from './../components/Layout';
import { useState, useEffect } from 'react';
const Clock = () => {

    const [state, setstate] = useState('');
    const [hours, sethours] = useState('');
    const [minutes, setminutes] = useState('');
    const [seconds, setseconds] = useState('');
    const [isstart, setStart] = useState(true);
    const [totalMili, setTotalMili] = useState(0)
    const [ inter, setInter] = useState(0);
    const [ isStop, setIstop] = useState(false);
    function setTime(num) {
        setstate(x => x + num)
        if (state.length < 2) {
            sethours(hours => hours + num)
        } else if (state.length < 4) {
            setminutes(min => min + num)
        } else if (state.length < 6) {
            setseconds(seconds => seconds + num)
            
        }

    }

    function run(){
        if(state.length >= 6){
            setStart(!isstart);
            setIstop(true);
            getTotalMiliseconds();
        }
    }

    const getTotalMiliseconds = () => {
        let total= ((parseInt(hours) * 60 * 60 + parseInt(minutes)*60 + parseInt(seconds))*1000);
        setTotalMili(total);
    }

    const countdown = (timeInMilliseconds) => {
        let ti = timeInMilliseconds;
        let hour = formatTime(Math.floor(ti/(60*60*1000)));
        ti = ti % (60*60*1000);
        let minu = formatTime(Math.floor(ti/(60*1000)));
        ti = ti % (60*1000);
        let sec = formatTime(Math.floor(ti/(1000)));
        ti = ti % 1000;
        const mili = formatTime(ti);
        if(totalMili>0){
            return `${hour}:${minu}:${sec}:${mili}`;
        }else{
            clearInterval(inter);
            return `00:00:00:00`;
        }
       
    }

    function formatTime(time){
        return (time <10 ? `0${time.toString().substring(0,2)}`: time.toString().substring(0,2));
    }

    function clearAll() {
        sethours('');
        setminutes('');
        setseconds('');
        setstate('');
    }

    function reset(){
        setStart(true)
        setIstop(false);
        clearInterval(inter);
        setTotalMili(0);
        setInter(null);
        sethours('');
        setminutes('');
        setseconds('');
        setstate('');
    }
    
    function clearOne(){
        if(seconds.length>0){
            setstate(x=>x.slice(-4));
            setseconds('')
        }else if(seconds.length<=0 && minutes.length>0){
           
            setstate(x=>x.slice(-2));
            setminutes('')
        }else if(minutes.length<=0){
            setstate('');
            sethours('')
        }
    }

    useEffect(() => {
        if(isstart && isStop){
            setInter(setInterval(()=>{
                setTotalMili(x=>x-50);
            }, 50));
            
        }
    }, [isstart]);
    useEffect(() => {
        if(!isStop){
            clearInterval(inter);
            setInter(null);
        }else{
            setInter(setInterval(()=>{
                setTotalMili(x=>x-50);
            }, 50));
         
        }
        
    }, [isStop]);

    function stop(act){
        setIstop(act);
    }

    return (
        <Layout>
            <div className="container">
                {isstart ?
                    <div className="time" style={{ marginTop: 'unset' }}>
                        <div className="timetitle">
                            <p>H</p>
                            <p>M</p>
                            <p>S</p>
                        </div>
                        <div className="timeview">

                            <span >{hours}</span>
                            <p>:</p>
                            <span> {minutes}</span>
                            <p>:</p>
                            <span>{seconds}</span>

                        </div>
                    </div>
                    :
                    <div className="time" style={{ backgroundColor: 'white', marginTop: '40%' }}>
                        <div className="timetitle">
                            <p>H</p>
                            <p>M</p>
                            <p>S</p>
                            <p>MS</p>
                        </div>
                        <div className="timeview">
                            <span>{countdown(totalMili)}</span>
                        </div>
                    </div>
                }
                {isstart ?
                    <div className="keyboard" style={{ marginTop: '48%' }}>
                        <table>
                            <tbody>
                                <tr>
                                    <td><button onClick={() => setTime(1)}>1</button></td>
                                    <td><button onClick={() => setTime(2)}>2</button></td>
                                    <td><button onClick={() => setTime(3)}>3</button></td>
                                </tr>
                                <tr>
                                    <td><button onClick={() => setTime(4)}>4</button></td>
                                    <td><button onClick={() => setTime(5)}>5</button></td>
                                    <td><button onClick={() => setTime(6)}>6</button></td>
                                </tr>
                                <tr>
                                    <td><button onClick={() => setTime(7)}>7</button></td>
                                    <td><button onClick={() => setTime(8)}>8</button></td>
                                    <td><button onClick={() => setTime(9)}>9</button></td>
                                </tr>
                                <tr>
                                    <td><button onClick={()=>clearOne()}>x</button></td>
                                    <td><button onClick={() => setTime(0)}>0</button></td>
                                    <td><button onClick={() => clearAll()}></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    :
                    <div className="keyboard"></div>
                }

                {isstart ?
                    <div className="button-bot">
                        <button onClick={() => run()}>Start</button>
                    </div>
                    :
                    <div className="button-bot button-bot-start">
                        <button style={{ backgroundColor: isStop? 'red':'green', color: 'white' }} onClick={ totalMili>0?()=>stop(!isStop):()=>stop(!isStop)}>{isStop? 'Stop':'Resume'}</button>
                        <button style={{ backgroundColor: 'blue', color: 'white' }} onClick={() => reset()}>Reset</button>
                    </div>
                }
            </div>
            <style jsx>{`
                .container{
                    height: 400px;
                    width: 250px;
                    border: 1px solid black;
                    position: relative;
                }
                .time{
                    position: absolute;
                    height: 30%;
                    width: 100%;
                    background-color: #e3e3ce;
                }
                .keyboard{
                    position: absolute;
                    margin-top: 85%;
                    height: 55%;
                    width: 100%;
                }
                .button-bot{
                    margin-top: 136%;
                    position: absolute;
                    height: 15%;
                    width: 100%;
                }
                .button-bot button{
                    width: 100%;
                    height: 100%;
                    background-color: green;
                    border: 0px;
                }
                .button-bot-start button{
                    width: 50%;
                    height: 100%;
                    background-color: green;
                    border: 0px;
                }
                table{
                    height: 100%;
                    width:100%;
                    background-color: #b6b67c;
                }
                table tr td button{
                    height: 100%;
                    width:100%;
                    background-color: white;
                    border-radius: 5px;
                }

                .timetitle{
                    width: 100%;
                    height:40%;
                    display: flex;
                    justify-content: space-around;
                }
                .timetitle p{
                    margin-top: 10%
                }
                .timeview{
                    width: 100%;
                    height:40%;
                    background-color: white;
                    justify-content: space-evenly;
                    display: flex;
                }
                .timeview span{
                    font-size: 40px;
                    font-weight: bold;
                    width: 100%;
                    text-align: center;
                }
                .timeview p{
                    font-size: 40px;
                    font-weight: bold;
                    margin: 0;
                }
            `}</style>
        </Layout>

    );
}


export default Clock;