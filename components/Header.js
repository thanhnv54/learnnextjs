import Link from 'next/link'
const linkStyle = {
  marginRight: 15,
  textDecoration:'none'
}

const contai ={
  display: 'flex',
  height: '50px',
  width: '100vw',
  justifyContent: 'center',
  
}

export default function Header() {
  return (
    <div style ={contai}>
      <Link href="/"  >
        <a style={linkStyle} >Clicker</a>
      </Link>
      <Link href="/timer1">
        <a style={linkStyle}>Timer 1</a>
      </Link>
      <Link href="/timer2">
        <a style={linkStyle} >Timer 2</a>
      </Link>
      <Link href="/calculator">
        <a style={linkStyle}>Calculator</a>
      </Link>
      <Link href="/clock">
        <a style={linkStyle} >Clock</a>
      </Link>
    </div>
  )
}