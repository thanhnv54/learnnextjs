import Header from './Header'

const layoutStyle = {
  margin: 0,
  padding: 0,
  border: '0px solid #DDD',
  display: 'flex',
  height: '600px',
  width: '100vw',
  justifyContent: 'center'
}

export default function Layout(props) {
  return (
    <div>
      <Header />
      <div style={layoutStyle}>
        {props.children}
      </div>
    </div>

  )
}